package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.alicebot.ab.*;

import java.util.Optional;


public class Controller {

    @FXML
    private TextArea chatArea, inputArea;

    @FXML
    private Button sendBtn;

    private Chat chatSession;

    public void initialize(final Stage stage)  {

        ChoiceDialog<String> dialog = new ChoiceDialog<>("abou", "super");
        dialog.setTitle("Bot selection");
        dialog.setHeaderText("Choose a bot");
        dialog.setContentText("Bot: ");
        Optional<String> result = dialog.showAndWait();



        chatArea.setText("This is a proof of concepts for a chatbot using program AB & JavaFX.\n" +
                "Type your message in the area below to get an answer from the bot.\n\n");
        chatArea.setEditable(false);
        chatArea.setWrapText(true);

        inputArea.setWrapText(true);
        inputArea.setPromptText("Type your message here...");
        inputArea.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ENTER) {
                sendMessage();
                keyEvent.consume();
            }
        });

        sendBtn.setText("Send");
        sendBtn.setOnAction(e -> sendMessage());

        // Initialize bot
        Bot bot = new Bot(result.get(), ".");
        chatSession = new Chat(bot);

    }

    private void sendMessage() {
        if (!inputArea.getText().isEmpty()) {
            String userMsg = inputArea.getText();
            appendToChatArea(String.format("User: %s", userMsg));
            inputArea.clear();
            askBot(userMsg);
        }
    }

    private void appendToChatArea(String message) {
        chatArea.appendText(message + "\n");
    }

    private void askBot(String message) {
        String response = chatSession.multisentenceRespond(message);
        appendToChatArea(String.format("Bot: %s", response));
    }

}
